import java.util.Scanner;
import java.util.Random;

public class Roulette{
    public static void main(String[] args){

        Scanner reader=new Scanner(System.in);

       RouletteWheel game=new RouletteWheel();
       int balance=1000;

      
       boolean keepPlaying=false;

       while(!keepPlaying){
        System.out.println("Place in your bet");
        int bet=reader.nextInt();

        while(bet>balance || bet<=0){
            System.out.println("Your bet surpasses your current balance or you bet 0 dollars, insert a new bet.");
            bet=reader.nextInt();
           }

        
    
           System.out.println("What Number are you betting?");
           int num=reader.nextInt();
    
           while(num<0 || num>36){
            System.out.println("Your number is out of the range, it should be between 1 and 36");
            num=reader.nextInt();
           }
    
           game.spin();
    
           if(game.getValue()==num){
            System.out.println("You got the right number! You won your bet!");
            balance=balance*35;
           }
           else{
            System.out.println("Sorry champ, better luck next time");
            balance=balance-bet;
           }

           System.out.println("Do you wanna play again? [y or n]");
           String again=reader.next();

           if(again.equals("n")){
            keepPlaying=true;
           }
       }

       System.out.println("Your total balance is " + balance);
       
    }

}